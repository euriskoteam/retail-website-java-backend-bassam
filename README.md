## Technology assessment README File
### Project Overview:
This project takes a Bill and outputs it's final amount after calculating certain discounts, when applied.

#### Rules and restrictions:  
  - If the user is an employee of the store, he gets 30% discount.  
  - If the user is an affiliate of the store, he gets a 10% discount.  
  - If the user has been a customer for over 2 years, he gets a 5% discount.  
  - For every 100$ on the bill, there would be a 5$ discount.  
  - The percentage based discounts do not apply on groceries.  
  - The user can get only one of the percentage based discounts on a bill.  
  

#### Structure:  
##### The Item class:  
Item is a class that represents the items from the super market. It has two data members. The first one is called "price" which is of type double, an the second is of type ItemType which is an enum.
For this assignment I have assigned three values for the enum ItemType: Clothes, Electronics and Groceries.
Each value of one of these types will have it's own price, which will be specified on creation of that Object.

##### The User class:
The "User" class is an abstract class that has one data member of type double called "percentage". As specified in the assignment, we have three classes that extend from "Discounts": Employee, Affiliate, Customer.
Each instance of one of these classes will have the same percentage assigned to that specific class, for examlpe all Employee objects will have the same discounts, which is specific for the type Employee.

##### The Bill class:
This class has two data members:    
  - An arrayList of Objects of type "ItemType".  
  - An Object of type "User".  

Bill has two methods:  
  - addToBill: Adds Items to the list.  
  - removeFromBill: Removes Items from the list.    
##### Services:  
The Services class has two methods:
  - discountPerHundred: This method calculates how many hundred dollars are in the bill amount and deducts a 5$ discount for each 100$.  
  - calculateBill: Checks for the type of each item in the Item list, if Item type is Grocery, percentage discount does not apply to the price of the Item, only the discountPerHundred applies to the Item price, and add that price to the total price. If the Item is from any other type, and benefits from a discount apply the discountPerHundred to each item and add their price to the total price. After having the total price, deduct 5$ of each 100$ contained in the final price.  

##### Running The test cases:
Test cases are generated in the ApplicationTest.java file. Several Objects are instantiated.
In order to test the project the user wll have to access the test file included in the project. This file contains a JUnit testing environment. The following tests are implmented in the testing environment.  
  - One Discount Object for each class that extends from Discounts.  
  - Two Objects Clothing.  
  - Two Objects from Electronics.  
  - Three Objects from Groceries.  
  - One Bill Object b1.  
  
The test file goes through seven test cases:  
  - b1 has the employee discount 30% and doesn't have groceries in his shopping list. 30% will be deduced from amount along with the 100$ discount.  
  -  b1 is now an affiliate he has a discount of 10% and doesn't have groceries in his shopping list.10% will be deduced from amount along with the 100$ discount.  
  - b1 is now and a customer for over 2 years he has a discount of 5% and doesn't have groceries in his shopping list.5% will be deduced from amount along with the 100$ discount.  
  - b1 is a new customer, he doesn't benefit from any discount.Only the 100$ discount applies.  
  - b1 is an employee and has 30% discount.30% will be deduced from amount along with the 100$ discount.  
  - b1 buys an apple with the iPhone however he does not get 30% on the apple, but other items will benefits from their 30% discount along with the 100$ discount.  
  - Testing if Bill made of groceries only will get the discount per 100$ without the percentage discount.  
   
In order to generate custom test cases manually, the user will have to:  
  - Create a new User.  
  - Create the Items that he wishes to add to the bill.  
  - Create a Bill and give it the user as parameter.  
  - Add the items created to the Bill.  
  - Instantiate an instance of the "Services" class.  
  - Call the method "calculateBill" from the service, give it the bill as paramater and store the result in a variable.  


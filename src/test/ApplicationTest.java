package test;

import model.*;
import services.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ApplicationTest {

	@Test
	void test() {
		User employee = new Employee();
		User affiliate = new Affiliate();
		User customer = new Customer(2);
		User customer1 = new Customer(1);

		Item shirt = new Item(90, ItemType.CLOTHING);
		Item pants = new Item(160, ItemType.CLOTHING);

		Item apple = new Item(25, ItemType.GROCERIES);
		Item banana = new Item(30, ItemType.GROCERIES);
		Item veryRareMango = new Item(100, ItemType.GROCERIES);

		Item iPhone = new Item(650, ItemType.ELECTRONICS);
		Item tv = new Item(500, ItemType.ELECTRONICS);

		Services svc = new Services();
		// Test 1: b1 has the employee discount 30%
		// and doesn't have groceries in his shopping list
		Bill b1 = new Bill(employee);
		b1.addToBill(shirt);
		b1.addToBill(pants);
		b1.addToBill(tv);
		double res = svc.calculateBill(b1);
		assertEquals(500.0, res);

//		//Test 2: b1 is now an affiliate he has a discount of 10%
//		//and doesn't have groceries in his shopping list
		b1 = new Bill(affiliate);
		b1.addToBill(shirt);
		b1.addToBill(pants);
		b1.addToBill(tv);
		res = svc.calculateBill(b1);
		assertEquals(645.0, res);
//		
//		//Test 3: b1 is now and a customer for over 2 years he has a discount of 5%
//		//and doesn't have groceries in his shopping list
		b1 = new Bill(customer);
		b1.addToBill(shirt);
		b1.addToBill(pants);
		b1.addToBill(tv);
		res = svc.calculateBill(b1);
		assertEquals(677.5, res);
//		
//		//Test 4: b1 is now and a customer for less than 2 years he does not have a discount of 5%
//		//and doesn't have groceries in his shopping list
		b1 = new Bill(customer1);
		b1.addToBill(shirt);
		b1.addToBill(pants);
		b1.addToBill(tv);
		res = svc.calculateBill(b1);
		assertEquals(715, res);
//				
//		//Test 5: b1 is an employee and has 30% discount
		b1 = new Bill(employee);
		b1.addToBill(iPhone);
		res = svc.calculateBill(b1);
		assertEquals(435.0, res);
//		
//		//Test 6: b1 buys an apple with the iPhone however he does not get 30% on the apple		
		b1.addToBill(apple);
		res = svc.calculateBill(b1);
		assertEquals(460, res);
//		
//		//Test 7: testing if groceries will get the discount per 100$ without the percentage discount
		b1.removeFromBill(iPhone);
		b1.addToBill(banana);
		b1.addToBill(veryRareMango);
		res = svc.calculateBill(b1);
		assertEquals(150, res);

	}

}

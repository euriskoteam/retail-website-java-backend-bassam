package services;

import model.Bill;

public class Services {

	public double discountPerHundred(double amount) {
		int disc = (int) amount / 100;
		amount -= 5 * disc;
		return amount;
	}

	public double calculateBill(Bill b) {
		double lastPrice = 0;

		for (int i = 0; i < b.getList().size(); i++) {
			// if list contains groceries, the groceries do not get a percentage discount
			// or if user is a regular customer with no discount
			if (b.getList().get(i).getItemType().equals("Groceries")) {

				lastPrice += b.getList().get(i).getPrice();
			} else {

				lastPrice += b.getList().get(i).getPrice()
						- b.getList().get(i).getPrice() * b.getUser().getPercentage();

			}

		}

		// for every 100$ there's a 5% Discount
		lastPrice = discountPerHundred(lastPrice);

		return lastPrice;
	}

}

package model;

public class Item {
	
private ItemType type;
private double price;

public Item(double p, ItemType t)
{
	setPrice(p);
	type = t;
}

public String getItemType()
{
	return type.getType();
}

public double getPrice() {
	return price;
}

public void setPrice(double price) {
		this.price = price;
	}
}

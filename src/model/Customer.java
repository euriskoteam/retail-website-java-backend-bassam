package model;

public class Customer extends User {
	private double years;

	public Customer(double y) {
		years = y;
		if (years >= 2) {
			setPercentage(0.05);
		} else {
			setPercentage(0.0);
		}
	}
}

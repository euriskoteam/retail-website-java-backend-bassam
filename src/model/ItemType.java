package model;

public enum ItemType {
	CLOTHING("Clothing"), ELECTRONICS("Electronics"), GROCERIES("Groceries");

	private final String type;

	ItemType(String t) {
		type = t;
	}

	public String getType() {
		return type;
	}

}

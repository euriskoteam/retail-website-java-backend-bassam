package model;

import java.util.ArrayList;
public class Bill {
	private ArrayList<Item> list;
	private User user;
	
	public Bill(User type)
	{
		setList(new ArrayList<Item>());
		setUser(type);
	}
	
	public void addToBill(Item i)
	{
		getList().add(i);
	}
	
	public void removeFromBill(Item i)
	{
		getList().remove(i);
	}

	public ArrayList<Item> getList() {
		return list;
	}

	public void setList(ArrayList<Item> list) {
		this.list = list;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
